# Command line tool

Реализация управления проектом Django через python fabric3.

## Fabric3

Модуль для python fabric3 - позволяет реализовывать переносимый command line tool, приемлемый для использования как в контексте docker, так и уже классического python virtualenv.

Запуск команд происходит через утилиту fab, установленную вместе с модулем fabric:

```
fab <command_name>
```

Также fabric для выполнения описанных команд можно передавать аргументы:

```
fab <comman_name>:<arg1>,<arg2>,<arg_name>=<arg_value>
```

## Полезная литература

[PYPI модуля](https://pypi.org/project/Fabric3/)

[Fabric tutorial](https://docs.fabfile.org/en/1.4.3/tutorial.html)

[django-admin and manage.py](https://docs.djangoproject.com/en/2.1/ref/django-admin/)
